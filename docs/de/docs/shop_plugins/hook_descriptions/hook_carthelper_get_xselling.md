# HOOK_CARTHELPER_GET_XSELLING (411)

## Triggerpunkt

Am Ende von getXSelling().

## Parameter
* `array` **oArtikelKey_arr** - Zusammenstellung der Artikel-IDs
* `array` **&xsellData** - Artikel-IDs für die Cross-Selling-Produkte