# HOOK_TOOLS_GLOBAL_CHECKEWARENKORBEINGANG_WUNSCHLISTE (167)

## Triggerpunkt

Nach dem Hinzufügen eines Artikels zur Wunschliste

## Parameter

* `int` **&kArtikel** - Artikel-ID
* `int` **&fAnzahl** - Artikelanzahl
* `stdClass` **&AktuellerArtikel** - der aktuelle Artikel