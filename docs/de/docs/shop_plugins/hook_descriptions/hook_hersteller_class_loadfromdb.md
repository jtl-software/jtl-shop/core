# HOOK_HERSTELLER_CLASS_LOADFROMDB (119)

## Triggerpunkt

Nach dem Laden eines Herstellers aus der Datenbank

## Parameter

* `Hersteller`(`array|int|object`) **&oHersteller** - Hersteller
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"
* `array` **cacheTags** - Umfang des Zwischenspeicherns