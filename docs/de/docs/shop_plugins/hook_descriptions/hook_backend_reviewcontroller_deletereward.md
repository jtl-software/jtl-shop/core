# HOOK_BACKEND_REVIEWCONTROLLER_DELETEREWARD (417)

## Triggerpunkt

Vor Datenbankupdate bei Löschen von Bewertungsguthaben.

## Parameter

* `JTL\Review\ReviewModel` **review** - Das geladene Model der Bewertung
* `JTL\Review\ReviewBonusModel` **reviewBonus** - Das geladene Model des Bewertungsguthabens
* `int` **customerID** - ID des betroffenen Kunden
