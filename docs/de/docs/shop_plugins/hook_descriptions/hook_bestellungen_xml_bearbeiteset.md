# HOOK_BESTELLUNGEN_XML_BEARBEITESET (159)

## Triggerpunkt

Nach dem "Füllen" einer Bestellung (in dbeS)

## Parameter

* `stdClass` **&oBestellung** - Bestellungsobjekt
* `JTL\Customer\Kunde` **&oKunde** - Kundenobjekt
* `stdClass` **&oBestellungWawi** - an die WaWi zu sendende Bestellung