# HOOK_REGISTRATION_CUSTOMER_CREATED (362)

## Triggerpunkt

Sobald ein Kunde während des Registrierungsprozesses in der Datenbank angelegt wurde

## Parameter

* `int` **customerID** - neu erstellte ID des Kunden