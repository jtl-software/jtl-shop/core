# HOOK_REVIEWMANAGER_ADDREWARD_START (415)

## Triggerpunkt

Am Anfang der Gutschrift für Bewertungen.

## Parameter

* `JTL\Review\ReviewModel` **review** - Das geladene Model der Bewertung
* `float` **reward** - Referenz auf das Guthaben
