# HOOK_ARTIKEL_INC_AEHNLICHEARTIKEL (72)

## Triggerpunkt

Kurz vor der Rückgabe der ähnlichen Artikel, in den Artikeldetails

## Parameter

* `int` **kArtikel** - Artikel-ID
* `array` **&oArtikel_arr** - Liste der ähnlichen Artikel