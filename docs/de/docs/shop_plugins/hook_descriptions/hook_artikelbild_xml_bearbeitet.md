# HOOK_ARTIKELBILD_XML_BEARBEITET

## Triggerpunkt

In `JTL\dbeS\Sync\ImageLink::handleInserts` nach dem Einfügen eines Bildes beim Abgleich mit JTL-Wawi.

## Parameter

* `int` **kArtikel** - ID des bearbeiteten Artikels
* `int` **kBild** - ID des eingefügten Bildes