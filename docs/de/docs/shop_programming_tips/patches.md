# Patches einspielen

Für neue Versionen von JTL Shop werden Änderungen am NOVA Template oder an den Email-Templates von uns üblicherweise
als Patch-Dateien im diff-Format zur Verfügung gestellt. Diese diff-Files können mit unterschiedlichen Tools in Ihren
eigenen Shop eingespielt werden:

* Mit [PhpStorm](https://www.jetbrains.com/help/phpstorm/using-patches.html#apply-patch) kann man über
  "Git -> Patch -> Apply Patch..." eine diff-Datei importieren.
* [Win Merge](https://winmerge.org/?lang=de) ist ein weiteres beliebtes Tool und bietet eine detaillierte Übersicht
  über die vorausstehenden Änderungen.
* Für Visual Studio Code gibt es die Erweiterung
  [Git Patch](https://marketplace.visualstudio.com/items?itemName=paragdiwan.gitpatch)
* Andere mögliche Tools: [UltraCompare](https://www.ultraedit.com/catalog/ultracompare/) (kostenpflichtig),
  [Meld](https://meldmerge.org/), GitLab, Notepad++