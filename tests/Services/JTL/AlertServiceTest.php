<?php

declare(strict_types=1);

namespace Tests\Services\JTL;

use JTL\Alert\Alert;
use JTL\Services\JTL\AlertService;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\TestCase;

class AlertServiceTest extends TestCase
{
    public function testInstanceCreation(): AlertService
    {
        $as = new AlertService();
        $this->assertInstanceOf(AlertService::class, $as);

        return $as;
    }

    #[depends('testInstanceCreation')]
    public function testAddError(AlertService $as): AlertService
    {
        $alert = $as->addError('Das ist ein Test', 'testKey', [
            'SaveInSession' => true,
        ]);
        $this->assertInstanceOf(Alert::class, $alert);
        $this->assertEquals($alert, $as->getAlert('testKey'));
        $this->assertEquals(Alert::TYPE_ERROR, $as->getAlert('testKey')->getType());

        /** @var Alert $sessionAlert */
        $sessionAlert = \unserialize($_SESSION['alerts']['testKey'], ['allowed_classes', Alert::class]);
        $this->assertEquals('Das ist ein Test', $sessionAlert->getMessage());

        return $as;
    }

    #[depends('testAddError')]
    public function testRemoveAlertByKey(AlertService $as): void
    {
        $this->assertInstanceOf(Alert::class, $as->getAlert('testKey'));
        $as->removeAlertByKey('testKey');
        $this->assertNull($as->getAlert('testKey'));
        $this->assertNull($_SESSION['alerts']['testKey'] ?? null);
    }
}
