<?php

declare(strict_types=1);

namespace Tests\Redirect;

use JTL\Redirect\URLParser;
use Tests\UnitTestCase;

class URLParserTest extends UnitTestCase
{
    public function testInvalidShopURLException(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid shop URL');
        new URLParser('http://example.com', '///');
    }

    public function testException(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid URL: ///');
        new URLParser('///');
    }

    public function testEmptyURL(): void
    {
        $parser   = new URLParser('', 'http://example.com');
        $expected = [
            'scheme'   => null,
            'user'     => null,
            'pass'     => null,
            'host'     => null,
            'port'     => null,
            'path'     => null,
            'query'    => null,
            'fragment' => null,
        ];
        $this->assertSame($expected, $parser->toArray());
    }

    public function testDifferentShopHost(): void
    {
        $parser   = new URLParser('http://example.net/subpath', 'http://example.net/testpath');
        $expected = [
            'scheme'   => 'http',
            'user'     => null,
            'pass'     => null,
            'host'     => 'example.net',
            'port'     => null,
            'path'     => '/subpath',
            'query'    => null,
            'fragment' => null,
        ];
        $this->assertSame($expected, $parser->toArray());

        $parser   = new URLParser('http://example.net/testpath', 'http://example.net/');
        $expected = [
            'scheme'   => 'http',
            'user'     => null,
            'pass'     => null,
            'host'     => 'example.net',
            'port'     => null,
            'path'     => '/testpath',
            'query'    => 'notrack',
            'fragment' => null,
        ];
        $this->assertSame($expected, $parser->toArray());

        $parser   = new URLParser('http://example.net/testpath/subpath', 'http://example.net/testpath');
        $expected = [
            'scheme'   => 'http',
            'user'     => null,
            'pass'     => null,
            'host'     => 'example.net',
            'port'     => null,
            'path'     => '/testpath/subpath',
            'query'    => 'notrack',
            'fragment' => null,
        ];
        $this->assertSame($expected, $parser->toArray());

        $parser   = new URLParser('example', 'http://example.net/testpath');
        $expected = [
            'scheme'   => 'http',
            'user'     => null,
            'pass'     => null,
            'host'     => 'example.net',
            'port'     => null,
            'path'     => '/testpath/example',
            'query'    => 'notrack',
            'fragment' => null,
        ];
        $this->assertSame($expected, $parser->toArray());

        $parser = new URLParser('/example', 'http://example.net/testpath/');
        $this->assertSame($expected, $parser->toArray());

        $parser           = new URLParser('/example/', 'http://example.net/testpath//');
        $expected['path'] = '/testpath/example/';
        $this->assertSame($expected, $parser->toArray());

        $parser            = new URLParser('?foo=42', 'http://example.net/testpath/');
        $expected['path']  = '/testpath/';
        $expected['query'] = 'foo=42&notrack';
        $this->assertSame($expected, $parser->toArray());

        $parser           = new URLParser('/example?foo=42', 'http://example.net/testpath//');
        $expected['path'] = '/testpath/example';
        $this->assertSame($expected, $parser->toArray());
    }

    public function testURLs(): void
    {
        $parser   = new URLParser('http://example.com');
        $expected = [
            'scheme'   => 'http',
            'user'     => null,
            'pass'     => null,
            'host'     => 'example.com',
            'port'     => null,
            'path'     => null,
            'query'    => null,
            'fragment' => null,
        ];
        $this->assertSame($expected, $parser->toArray());

        $parser   = new URLParser('https://nobody:pa$$w0ord@example.com:8088/path/p2?testquery#testfragment');
        $expected = [
            'scheme'   => 'https',
            'user'     => 'nobody',
            'pass'     => 'pa$$w0ord',
            'host'     => 'example.com',
            'port'     => 8088,
            'path'     => '/path/p2',
            'query'    => 'testquery',
            'fragment' => 'testfragment',
        ];
        $this->assertSame($expected, $parser->toArray());
    }
}
