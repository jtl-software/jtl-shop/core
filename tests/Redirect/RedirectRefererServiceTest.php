<?php

declare(strict_types=1);

namespace Tests\Redirect;

use InvalidArgumentException;
use JTL\DB\DbInterface;
use JTL\DB\NiceDB;
use JTL\Redirect\Repositories\RedirectRefererRepository;
use JTL\Redirect\Services\RedirectRefererService;
use Tests\UnitTestCase;

class RedirectRefererServiceTest extends UnitTestCase
{
    private DbInterface $db;

    private RedirectRefererService $redirectRefererService;

    private RedirectRefererRepository $redirectRefererRepository;

    /**
     * This method is called before each test.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        $this->db                        = $this->createStub(NiceDB::class);
        $this->redirectRefererRepository = $this->getMockBuilder(RedirectRefererRepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([$this->db])
            ->onlyMethods(['getReferers'])
            ->getMock();
        $this->redirectRefererService    = new RedirectRefererService($this->redirectRefererRepository);
    }

    public function testGetRepository(): void
    {
        $this->assertEquals($this->redirectRefererRepository, $this->redirectRefererService->getRepository());
    }

    public function testDeleteByID(): void
    {
        $this->db->method('deleteRow')
            ->willReturn(1);
        $this->assertTrue($this->redirectRefererService->deleteByID(1));
    }

    public function testDeleteByIDFailed(): void
    {
        $this->db->method('deleteRow')
            ->willReturn(-1);
        $this->assertFalse($this->redirectRefererService->deleteByID(1));
    }

    public function testCreateDTO(): void
    {
        $dto = $this->redirectRefererService->createDO(1, 1, 'https://google.com', '192.168.1.1');
        $this->assertObjectHasProperty('redirectID', $dto);
        $this->assertObjectHasProperty('botID', $dto);
        $this->assertObjectHasProperty('url', $dto);
        $this->assertObjectHasProperty('ip', $dto);
        $this->assertEquals('https://google.com', $dto->url);
        $this->assertEquals(1, $dto->redirectID);
    }

    public function testGetByIDFail(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->redirectRefererService->getByID(1);
    }

    public function testGetByID(): void
    {
        $this->db->method('select')
            ->willReturn(
                (object)[
                    'kRedirectReferer' => 10,
                    'kRedirect'        => 1,
                    'kBesucherBot'     => 1,
                    'cRefererUrl'      => 'https://google.com',
                    'cIP'              => '192.168.1.1',
                    'dDate'            => '1669156069'
                ]
            );
        $res = $this->redirectRefererService->getByID(10);
        $this->assertEquals(10, $res->id);
        $this->assertEquals(1, $res->redirectID);
        $this->assertEquals('192.168.1.1', $res->ip);
    }

    public function testGetRefererByIPAndURL(): void
    {
        $this->assertNull($this->redirectRefererService->getRefererByIPAndURL('', ''));
        $this->db->method('getSingleObject')
            ->willReturn(
                (object)[
                    'kRedirectReferer' => 10,
                    'kRedirect'        => 1,
                    'kBesucherBot'     => 1,
                    'cRefererUrl'      => 'https://google.com',
                    'cIP'              => '192.168.1.1',
                    'dDate'            => '1669156069'
                ]
            );
        $this->assertIsObject($this->redirectRefererService->getRefererByIPAndURL('192.168.1.1', 'https://google.com'));
    }
}
