<?php

declare(strict_types=1);

namespace Tests\Redirect;

use InvalidArgumentException;
use JTL\DB\DbInterface;
use JTL\DB\NiceDB;
use JTL\Redirect\Repositories\RedirectRepository;
use JTL\Redirect\Services\RedirectService;
use Tests\UnitTestCase;

class RedirectServiceTest extends UnitTestCase
{
    private DbInterface $db;

    private RedirectService $redirectService;

    private RedirectRepository $redirectRepository;

    /**
     * This method is called before each test.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        $this->db                 = $this->createStub(NiceDB::class);
        $this->redirectRepository = $this->getMockBuilder(RedirectRepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([$this->db])
            ->onlyMethods(['getRedirects', 'getItemBySource'])
            ->getMock();
        $this->redirectService    = new RedirectService($this->redirectRepository);
    }

    public function testGetRepository(): void
    {
        $this->assertEquals($this->redirectRepository, $this->redirectService->getRepository());
    }

    public function testDeleteByID(): void
    {
        $this->db->method('deleteRow')
            ->willReturn(1);
        $this->assertTrue($this->redirectService->deleteByID(1));
    }

    public function testDeleteByIDFail(): void
    {
        $this->db->method('deleteRow')
            ->willReturn(-1);
        $this->assertFalse($this->redirectService->deleteByID(1));
    }

    public function testDeleteUnassigned(): void
    {
        $this->db->method('getAffectedRows')
            ->willReturn(20);
        $this->assertEquals(20, $this->redirectService->deleteUnassigned());
    }

    public function testCreateDTO(): void
    {
        $dto = $this->redirectService->createDO('sourceTest', 'destinationTest', 1, 0, 123);
        $this->assertObjectHasProperty('source', $dto);
        $this->assertObjectHasProperty('destination', $dto);
        $this->assertObjectHasProperty('id', $dto);
        $this->assertObjectHasProperty('dateCreated', $dto);
        $this->assertObjectHasProperty('available', $dto);
        $this->assertObjectHasProperty('paramHandling', $dto);
        $this->assertObjectHasProperty('type', $dto);
        $this->assertObjectHasProperty('count', $dto);
        $this->assertEquals('sourceTest', $dto->source);
        $this->assertEquals(123, $dto->id);
    }

    public function testGetTotalCount(): void
    {
        $this->db->method('getSingleInt')
            ->willReturn(23);
        $this->assertEquals(23, $this->redirectService->getTotalCount());
    }

    public function testSaveExceptionSourceDestination(): void
    {
        $dto = $this->redirectService->createDO('test', 'test', 1, 0, 1);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Source and destination must not be the same');
        $this->redirectService->save($dto);

        $dto = $this->redirectService->createDO('', 'destination', 1, 0, 1);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid source or destination');
        $this->redirectService->save($dto);

        $dto = $this->redirectService->createDO('source', '', 1, 0, 1);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid source or destination');
        $this->redirectService->save($dto);
    }

    public function testSaveFail(): void
    {
        $service = $this->getMockBuilder(RedirectService::class)
            ->enableOriginalConstructor()
            ->onlyMethods(['getHeaders'])
            ->setConstructorArgs([$this->redirectRepository])
            ->getMock();
        $service->method('getHeaders')
            ->willReturn(['HTTP/1.1 200 OK']);
        $this->redirectRepository->expects($this->once())
            ->method('getItemBySource')
            ->willReturn(null);
        $dto = $service->createDO('/source', '/destination', 1, 0, 1);
        $this->assertFalse($service->save($dto));
    }

    public function testSaveOK(): void
    {
        $service = $this->getMockBuilder(RedirectService::class)
            ->enableOriginalConstructor()
            ->onlyMethods(['getHeaders'])
            ->setConstructorArgs([$this->redirectRepository])
            ->getMock();
        $service->method('getHeaders')
            ->willReturn(['HTTP/1.1 200 OK']);
        $this->db->method('insertRow')
            ->willReturn(1);
        $this->redirectRepository->expects($this->once())
            ->method('getItemBySource')
            ->willReturn(null);
        $dto = $service->createDO('/source', '/destination', 1, 0, 1);
        $this->assertTrue($service->save($dto));
    }

    public function testSaveExistsOverwrite(): void
    {
        $service = $this->getMockBuilder(RedirectService::class)
            ->enableOriginalConstructor()
            ->onlyMethods(['getHeaders'])
            ->setConstructorArgs([$this->redirectRepository])
            ->getMock();
        $service->method('getHeaders')->willReturn(['HTTP/1.1 200 OK']);
        $this->db->method('insertRow')->willReturn(1);
        $this->db->method('update')->willReturn(1, 0);
        $this->redirectRepository->method('getItemBySource')
            ->willReturn((object)['kRedirect' => 1, 'cFromUrl' => '/source', 'cToUrl' => '/destination']);
        $dto = $service->createDO('/source', '/destination3', 1, 0, 1);
        $this->assertFalse($service->save($dto));
        // first db->update() will return 1
        $this->assertTrue($service->save($dto, false, true));
        // second db->update() will return 0
        $this->assertFalse($service->save($dto, false, true));
    }

    public function testGetByIDNotExists(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Redirect not found');
        $this->redirectService->getByID(123456789);
    }

    public function testGetByID(): void
    {
        $this->db->method('select')
            ->willReturn(
                (object)[
                    'kRedirect'     => 123,
                    'cFromUrl'      => '/source',
                    'cToUrl'        => '/destination',
                    'nCount'        => 1,
                    'cAvailable'    => 'y',
                    'paramHandling' => 0,
                    'type'          => 0,
                ]
            );
        $this->assertEquals(123, $this->redirectService->getByID(1)->id);
    }

    public function testGetRedirects(): void
    {
        $redirects = [
            (object)['kRedirect' => 1, 'cFromUrl' => 'source1', 'cToUrl' => 'destination1', 'nCount' => 1],
            (object)['kRedirect' => 2, 'cFromUrl' => 'source2', 'cToUrl' => 'destination2', 'nCount' => 2],
            (object)['kRedirect' => 3, 'cFromUrl' => 'source3', 'cToUrl' => 'destination3', 'nCount' => 2],
        ];
        $map       = [
            ['', '', '', $redirects],
            ['', '', '2', [$redirects[0], $redirects[1]]],
        ];
        $this->redirectRepository->method('getRedirects')->willReturnMap($map);

        $this->assertCount(3, $this->redirectService->getRedirects());
        $this->assertCount(2, $this->redirectService->getRedirects('', '', '2'));
    }

    public function testCheckAvailability(): void
    {
        $service = $this->getMockBuilder(RedirectService::class)
            ->enableOriginalConstructor()
            ->onlyMethods(['getHeaders'])
            ->setConstructorArgs([$this->redirectRepository])
            ->getMock();
        $service->method('getHeaders')
            ->willReturnOnConsecutiveCalls(
                ['HTTP/1.1 200 OK'],
                ['HTTP/1.1 404 Not Found'],
                ['HTTP/1.1 200 OK'],
                ['HTTP/1.1 404 Not Found']
            );
        $this->assertTrue($service->checkAvailability('/exampleFound'));
        $this->assertFalse($service->checkAvailability('/exampleNotFound'));
        $this->assertFalse($service->checkAvailability('///'));
        $this->assertTrue($service->checkAvailability('https://example.com/'));
        $this->assertFalse($service->checkAvailability('https://example.com/notfound'));
    }
}
