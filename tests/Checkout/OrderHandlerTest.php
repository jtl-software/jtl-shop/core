<?php

declare(strict_types=1);

namespace Tests\Checkout;

use JTL\Cart\Cart;
use JTL\Checkout\OrderHandler;
use JTL\Customer\Customer;
use JTL\DB\NiceDB;
use JTL\Shop;
use JTL\Shopsetting;
use Psr\Log\LoggerInterface;
use PHPUnit\Framework\Attributes\Depends;
use Tests\UnitTestCase;

class OrderHandlerTest extends UnitTestCase
{
    private static array $settings;

    private static LoggerInterface $oldLogger;

    private static MockLogger $logger;

    private function setPrefix(string $prefix): void
    {
        self::$settings['bestellabschluss_bestellnummer_praefix'] = $prefix;
        Shopsetting::getInstance()->offsetSet('kaufabwicklung', self::$settings);
    }

    private function setSuffix(string $suffix): void
    {
        self::$settings['bestellabschluss_bestellnummer_suffix'] = $suffix;
        Shopsetting::getInstance()->offsetSet('kaufabwicklung', self::$settings);
    }

    private function setNumber(string $number): void
    {
        self::$settings['bestellabschluss_bestellnummer_anfangsnummer'] = $number;
        Shopsetting::getInstance()->offsetSet('kaufabwicklung', self::$settings);
    }

    /**
     * @inheritdoc
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $shopSettings   = Shopsetting::getInstance();
        self::$settings = $shopSettings->offsetGet('kaufabwicklung');

        self::$logger    = new MockLogger('mockLogger');
        self::$oldLogger = Shop::Container()->offsetGet(LoggerInterface::class);
        Shop::Container()->offsetUnset(LoggerInterface::class);
        Shop::Container()->singleton(LoggerInterface::class, fn() => self::$logger);
    }

    /**
     * @inheritdoc
     */
    public static function tearDownAfterClass(): void
    {
        Shop::Container()->offsetSet(LoggerInterface::class, self::$oldLogger);

        parent::tearDownAfterClass();
    }

    public function testInstanceCreation(): OrderHandler
    {
        $db           = $this->createMock(NiceDB::class);
        $customer     = $this->createMock(Customer::class);
        $cart         = $this->createMock(Cart::class);
        $orderHandler = new OrderHandler($db, $customer, $cart);
        $this->assertInstanceOf(OrderHandler::class, $orderHandler);

        return $orderHandler;
    }

    #[depends('testInstanceCreation')]
    public function testCreateOrderNo(OrderHandler $handler): void
    {
        $this->setPrefix('abcd');
        $this->setNumber('100');
        $this->setSuffix('xyz');
        $orderNr = $handler->createOrderNo();
        $this->assertEquals('abcd100xyz', $orderNr);

        $this->setPrefix('20230726_');
        $this->setNumber('11425');
        $this->setSuffix('_ABCDEFG');
        self::$logger->reset();
        $orderNr = $handler->createOrderNo();
        $this->assertEquals('20230726_11425_ABCDE', $orderNr);
        $this->assertEquals(
            'length of order number exceeds limit - suffix will be shortened',
            self::$logger->getLastMessage()
        );

        $this->setPrefix('20230726123456_');
        $this->setNumber('1142589');
        self::$logger->reset();
        $orderNr = $handler->createOrderNo();
        $this->assertEquals('230726123456_1142589', $orderNr);
        $this->assertEquals(
            'length of order number exceeds limit - prefix will be shortened',
            self::$logger->getLastMessage()
        );
    }

    #[depends('testInstanceCreation')]
    public function testCreateOrderNoWithParam(OrderHandler $handler): void
    {
        $this->setPrefix('abcd');
        $this->setNumber('100');
        $this->setSuffix('xyz');
        $orderNr = $handler->createOrderNo('abcd100xyz');
        $this->assertEquals('abcd100xyz', $orderNr);
        $orderNr = $handler->createOrderNo('thisshouldnotbereturned');
        $this->assertEquals('abcd100xyz', $orderNr);
        $orderNr = $handler->createOrderNo('abcd200xyz');
        $this->assertEquals('abcd200xyz', $orderNr);
    }

    #[depends('testInstanceCreation')]
    public function testCreateOrderNoWithOutSuffix(OrderHandler $handler): void
    {
        $this->setPrefix('abcd');
        $this->setNumber('100');
        $this->setSuffix('');
        $orderNr = $handler->createOrderNo('abcd100');
        $this->assertEquals('abcd100', $orderNr);
        $orderNr = $handler->createOrderNo('abcd200');
        $this->assertEquals('abcd200', $orderNr);
        $orderNr = $handler->createOrderNo('thisshouldnotbereturned');
        $this->assertEquals('abcd100', $orderNr);
    }

    #[depends('testInstanceCreation')]
    public function testCreateOrderNoWithOutPraefix(OrderHandler $handler): void
    {
        $this->setPrefix('');
        $this->setNumber('100');
        $this->setSuffix('');
        $orderNr = $handler->createOrderNo('100');
        $this->assertEquals('100', $orderNr);
        $orderNr = $handler->createOrderNo('200');
        $this->assertEquals('200', $orderNr);
        $orderNr = $handler->createOrderNo('thisshouldnotbereturned');
        $this->assertEquals('100', $orderNr);
    }
}
