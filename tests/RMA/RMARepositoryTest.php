<?php

declare(strict_types=1);

namespace Tests\RMA;

use Illuminate\Support\Collection;
use JTL\DB\DbInterface;
use JTL\DB\NiceDB;
use JTL\RMA\DomainObjects\RMADomainObject;
use JTL\RMA\DomainObjects\RMAItemDomainObject;
use JTL\RMA\Repositories\RMARepository;
use PHPUnit\Framework\Attributes\DataProvider;
use ReflectionMethod;
use Tests\UnitTestCase;

class RMARepositoryTest extends UnitTestCase
{
    private DbInterface $db;

    private RMARepository $rmaRepository;

    /**
     * This method is called before each test.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->db            = $this->createStub(NiceDB::class);
        $this->rmaRepository = $this->getMockBuilder(RMARepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([$this->db])
            ->onlyMethods(['initArtikel'])
            ->getMock();
    }

    /**
     * @return array[]
     * @comment Must return an array of arrays or an object that implements the Iterator interface.
     */
    public static function dataProviderRMAs(): array
    {
        return [
            [
                [
                    [
                        'id'                         => 0,
                        'return_address_id'          => 0,
                        'rma_id'                     => 0,
                        'customerID'                 => 0,
                        'return_address_firstName'   => '',
                        'return_address_lastName'    => '',
                        'return_address_street'      => '',
                        'return_address_houseNumber' => '',
                        'return_address_postalCode'  => '',
                        'return_address_city'        => '',
                        'return_address_state'       => '',
                        'return_address_countryISO'  => '',
                        'productID'                  => 0,
                        'isDivisible'                => 'N',
                        'rma_item_id'                => 0,
                        'name'                       => '',
                        'unitPriceNet'               => 0.00,
                        'rma_synced'                 => false,
                        'rma_status'                 => '',
                        'shippingNotePosID'          => 1,
                        'wawiID'                     => 0,
                        'replacementOrderID'         => 0,
                        'voucherCredit'              => false,
                        'refundShipping'             => false,
                        'orderPosID'                 => 0,
                        'orderID'                    => 0,
                        'reasonID'                   => 0,
                        'variationProductID'         => 0,
                        'partListProductID'          => 0,
                        'quantity'                   => 1,
                        'vat'                        => 0.00,
                        'rma_reasons_id'             => 0,
                        'rma_reasons_wawiID'         => 0,
                        'rma_reasons_lang_id'        => 0,
                        'rma_reasons_lang_reasonID'  => 0,
                        'rma_reasons_lang_langID'    => 0,
                        'shippingNoteID'             => 0,
                        'rma_createDate'             => '2021-01-01 00:00:00',
                        'shippingAddressID'          => 0,
                    ]
                ]
            ],
            [
                [
                    [
                        'id'                         => 1,
                        'return_address_id'          => 0,
                        'rma_id'                     => 1,
                        'customerID'                 => 0,
                        'return_address_firstName'   => '',
                        'return_address_lastName'    => '',
                        'return_address_street'      => '',
                        'return_address_houseNumber' => '',
                        'return_address_postalCode'  => '',
                        'return_address_city'        => '',
                        'return_address_state'       => '',
                        'return_address_countryISO'  => '',
                        'productID'                  => 0,
                        'isDivisible'                => 'N',
                        'rma_item_id'                => 0,
                        'name'                       => '',
                        'unitPriceNet'               => 0.00,
                        'rma_synced'                 => false,
                        'rma_status'                 => '',
                        'shippingNotePosID'          => 1,
                        'wawiID'                     => 0,
                        'replacementOrderID'         => 0,
                        'voucherCredit'              => false,
                        'refundShipping'             => false,
                        'orderPosID'                 => 0,
                        'orderID'                    => 0,
                        'reasonID'                   => 0,
                        'variationProductID'         => 0,
                        'partListProductID'          => 0,
                        'quantity'                   => 1,
                        'vat'                        => 0.00,
                        'rma_reasons_id'             => 0,
                        'rma_reasons_wawiID'         => 0,
                        'rma_reasons_lang_id'        => 0,
                        'rma_reasons_lang_reasonID'  => 0,
                        'rma_reasons_lang_langID'    => 0,
                        'shippingNoteID'             => 0,
                        'rma_createDate'             => '2021-01-01 00:00:00',
                        'shippingAddressID'          => 0,
                    ]
                ]
            ],
            [
                [
                    [
                        'id'                         => 0,
                        'return_address_id'          => 0,
                        'rma_id'                     => 0,
                        'customerID'                 => 0,
                        'return_address_firstName'   => '',
                        'return_address_lastName'    => '',
                        'return_address_street'      => '',
                        'return_address_houseNumber' => '',
                        'return_address_postalCode'  => '',
                        'return_address_city'        => '',
                        'return_address_state'       => '',
                        'return_address_countryISO'  => '',
                        'productID'                  => 0,
                        'isDivisible'                => 'N',
                        'rma_item_id'                => 0,
                        'name'                       => '',
                        'unitPriceNet'               => 0.00,
                        'rma_synced'                 => false,
                        'rma_status'                 => '',
                        'shippingNotePosID'          => 1,
                        'wawiID'                     => 0,
                        'replacementOrderID'         => 0,
                        'voucherCredit'              => false,
                        'refundShipping'             => false,
                        'orderPosID'                 => 0,
                        'orderID'                    => 0,
                        'reasonID'                   => 0,
                        'variationProductID'         => 0,
                        'partListProductID'          => 0,
                        'quantity'                   => 1,
                        'vat'                        => 0.00,
                        'rma_reasons_id'             => 0,
                        'rma_reasons_wawiID'         => 0,
                        'rma_reasons_lang_id'        => 0,
                        'rma_reasons_lang_reasonID'  => 0,
                        'rma_reasons_lang_langID'    => 0,
                        'shippingNoteID'             => 0,
                        'rma_createDate'             => '2021-01-01 00:00:00',
                        'shippingAddressID'          => 0,
                    ],
                    [
                        'id'                         => 1,
                        'return_address_id'          => 0,
                        'rma_id'                     => 1,
                        'customerID'                 => 0,
                        'return_address_firstName'   => '',
                        'return_address_lastName'    => '',
                        'return_address_street'      => '',
                        'return_address_houseNumber' => '',
                        'return_address_postalCode'  => '',
                        'return_address_city'        => '',
                        'return_address_state'       => '',
                        'return_address_countryISO'  => '',
                        'productID'                  => 0,
                        'isDivisible'                => 'N',
                        'rma_item_id'                => 0,
                        'name'                       => '',
                        'unitPriceNet'               => 2.00,
                        'rma_synced'                 => false,
                        'rma_status'                 => '',
                        'shippingNotePosID'          => 1,
                        'wawiID'                     => 0,
                        'replacementOrderID'         => 0,
                        'voucherCredit'              => false,
                        'refundShipping'             => false,
                        'orderPosID'                 => 0,
                        'orderID'                    => 0,
                        'reasonID'                   => 0,
                        'variationProductID'         => 0,
                        'partListProductID'          => 0,
                        'quantity'                   => 1,
                        'vat'                        => 0.00,
                        'rma_reasons_id'             => 0,
                        'rma_reasons_wawiID'         => 0,
                        'rma_reasons_lang_id'        => 0,
                        'rma_reasons_lang_reasonID'  => 0,
                        'rma_reasons_lang_langID'    => 0,
                        'shippingNoteID'             => 0,
                        'rma_createDate'             => '2021-01-01 00:00:00',
                        'shippingAddressID'          => 0,
                    ],
                    [
                        'id'                         => 2,
                        'return_address_id'          => 0,
                        'rma_id'                     => 2,
                        'customerID'                 => 0,
                        'return_address_firstName'   => '',
                        'return_address_lastName'    => '',
                        'return_address_street'      => '',
                        'return_address_houseNumber' => '',
                        'return_address_postalCode'  => '',
                        'return_address_city'        => '',
                        'return_address_state'       => '',
                        'return_address_countryISO'  => '',
                        'productID'                  => 0,
                        'isDivisible'                => 'N',
                        'rma_item_id'                => 0,
                        'name'                       => '',
                        'unitPriceNet'               => 1.00,
                        'rma_synced'                 => false,
                        'rma_status'                 => '',
                        'shippingNotePosID'          => 1,
                        'wawiID'                     => 0,
                        'replacementOrderID'         => 0,
                        'voucherCredit'              => false,
                        'refundShipping'             => false,
                        'orderPosID'                 => 0,
                        'orderID'                    => 0,
                        'reasonID'                   => 0,
                        'variationProductID'         => 0,
                        'partListProductID'          => 0,
                        'quantity'                   => 0,
                        'vat'                        => 0.00,
                        'rma_reasons_id'             => 0,
                        'rma_reasons_wawiID'         => 0,
                        'rma_reasons_lang_id'        => 0,
                        'rma_reasons_lang_reasonID'  => 0,
                        'rma_reasons_lang_langID'    => 0,
                        'shippingNoteID'             => 0,
                        'rma_createDate'             => '2021-01-01 00:00:00',
                        'shippingAddressID'          => 0,
                    ]
                ]
            ]
        ];
    }

    public function testGetTableName(): void
    {
        $this->assertSame(expected: 'rma', actual: $this->rmaRepository->getTableName());
    }

    /**
     * @throws \ReflectionException
     */
    public function testBuildQuery(): void
    {
        // buildQuery() is a private, but important method. Use ReflectionMethod to test it.
        $method  = new ReflectionMethod(objectOrMethod: $this->rmaRepository, method: 'buildQuery');
        $queries = [
            'emptyArray' => $method->invoke($this->rmaRepository, 1, []),
            'customerID' => $method->invoke($this->rmaRepository, 1, ['customerID' => 1]),
            'id'         => $method->invoke($this->rmaRepository, 1, ['id' => 1]),
            'status'     => $method->invoke($this->rmaRepository, 1, ['status' => 1]),
            'beforeDate' => $method->invoke($this->rmaRepository, 1, ['beforeDate' => 1]),
            'pID'        => $method->invoke($this->rmaRepository, 1, ['product' => 1]),
            'sID'        => $method->invoke($this->rmaRepository, 1, ['shippingNote' => 1]),
            'synced'     => $method->invoke($this->rmaRepository, 1, ['synced' => 1])
        ];

        $this->assertObjectHasProperty(propertyName: 'stmnt', object: $queries['emptyArray']);
        $this->assertObjectHasProperty(propertyName: 'params', object: $queries['emptyArray']);

        foreach ($queries as $key => $query) {
            if ($key === 'emptyArray') {
                continue;
            }
            // Check if the query contains ':key' as a placeholder
            $this->assertStringContainsString(needle: ':' . $key, haystack: $query->stmnt);
        }
    }

    /**
     * @description This test gets called three times. Two times with one RMA and one time with 3 RMAs. In the last
     * test run, the key 2 should exist if no limit is provided.
     * @comment The $filter parameter gets already tested in testBuildQuery().
     * @throws \Exception
     */
    #[DataProvider('dataProviderRMAs')]
    public function testGetReturns($rmas): void
    {
        $limit = 2;

        $getCollectionResult = new Collection();
        foreach ($rmas as $rma) {
            $getCollectionResult->add((object)$rma);
        }
        $this->db->method('getCollection')
            ->willReturn(value: $getCollectionResult);

        $returns = $this->rmaRepository->getReturns(
            langID: 1,
            limit: $limit
        );

        $this->assertContainsOnlyInstancesOf(className: RMADomainObject::class, haystack: $returns);
        $this->assertArrayNotHasKey(
            key: $limit,
            array: $returns,
            message: 'The limit on getReturns() doesnt seem to work as expected.'
        );
    }

    #[DataProvider('dataProviderRMAs')]
    public function testGetReturnableProducts($rmas): void
    {
        $collectionResult = new Collection();
        foreach ($rmas as $rma) {
            $collectionResult->add((object)$rma);
        }

        $this->db->method('getCollection')
            ->willReturn(value: $collectionResult);

        // Parameters affect SQL query directly. Run the integration test to get more insights.
        // The same applies for partListProductIDs. This cant be unit-tested without a real database.
        $returnableProducts = $this->rmaRepository->getReturnableProducts(
            customerID: 0,
            languageID: 0,
            cancellationTime: 0
        );

        $collectionResult->each(function ($item, $key) use ($returnableProducts) {
            if ($item->quantity > 0) {
                $this->assertInstanceOf(expected: RMAItemDomainObject::class, actual: $returnableProducts[$key]);
            } else {
                $this->assertNotTrue(isset($returnableProducts[$key]));
            }
        });
    }

    public function testGetOrderNumbers(): void
    {
        $this->db->method('getCollection')
            ->willReturn(
                value: new Collection(items: [
                    (object)['orderID' => 1, 'orderNo' => 'OrNu123'],
                    (object)['orderID' => 2, 'orderNo' => 'OrNu456'],
                    (object)['orderID' => 3, 'orderNo' => 'OrNu789']
                ])
            );

        $orderNumbers = $this->rmaRepository->getOrderNumbers(orderIDs: [1, 2, 3]);

        $this->assertEquals(expected: 'OrNu123', actual: $orderNumbers[1]);
        $this->assertEquals(expected: 'OrNu456', actual: $orderNumbers[2]);
        $this->assertEquals(expected: 'OrNu789', actual: $orderNumbers[3]);
    }

    /**
     * @throws \Exception
     */
    public function testMarkedAsSynced(): void
    {
        $this->db->method('getAffectedRows')
            ->willReturn(value: 1);

        $this->assertFalse($this->rmaRepository->markedAsSynced([]));
        $this->assertTrue(
            $this->rmaRepository->markedAsSynced(
                [
                    new RMADomainObject(id: 3),
                    new RMADomainObject(id: 7)
                ]
            )
        );
    }
}
