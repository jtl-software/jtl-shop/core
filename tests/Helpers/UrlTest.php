<?php

declare(strict_types=1);

namespace Tests\Helpers;

use JTL\Helpers\URL;
use PHPUnit\Framework\TestCase;

/**
 * Class TextTest
 * @package Tests\Helpers
 */
final class UrlTest extends TestCase
{
    private string $url = 'https://foo:bar@example.com:33/baz?q=22&z=aaa#foobar';

    private URL $helper;

    public function setUp(): void
    {
        $this->helper = new URL($this->url);
    }

    public function testGetUrl(): void
    {
        $this->assertSame($this->url, $this->helper->getUrl());
    }

    public function testGetScheme(): void
    {
        $this->assertSame('https', $this->helper->getScheme());
    }

    public function testGetHost(): void
    {
        $this->assertSame('example.com', $this->helper->getHost());
    }

    public function testGetPort(): void
    {
        $this->assertSame('33', $this->helper->getPort());
    }

    public function testGetUser(): void
    {
        $this->assertSame('foo', $this->helper->getUser());
    }

    public function testGetPass(): void
    {
        $this->assertSame('bar', $this->helper->getPass());
    }

    public function testGetPath(): void
    {
        $this->assertSame('/baz', $this->helper->getPath());
    }

    public function testGetQuery(): void
    {
        $this->assertSame('q=22&z=aaa', $this->helper->getQuery());
    }

    public function testGetFragment(): void
    {
        $this->assertSame('foobar', $this->helper->getFragment());
    }

    public function testNormalize(): void
    {
        $this->assertSame($this->url, $this->helper->normalize());
    }

    public function testUrlDecodeUnreservedChars(): void
    {
        $url = 'http://example.com/%7E%46%6F%6F/a-%5Fbx%2EABc%30%2D%31';
        $this->assertSame('http://example.com/~Foo/a-_bx.ABc0-1', $this->helper->urlDecodeUnreservedChars($url));
    }

    public function testRemoveDotSegments(): void
    {
        $url = 'http://example.com/a/b/../c/../';
        $this->assertSame('http://example.com/a/', $this->helper->removeDotSegments($url));
    }

    public function testSetters(): void
    {
        $scheme   = 'http';
        $host     = 'example.net';
        $port     = '8088';
        $fragment = 'testfragment';
        $password = 'p4$$w0ord';
        $user     = 'testuser';
        $query    = 'a=42&b=aaa';
        $path     = '/testpath';
        $helper   = new URL();
        $helper->setUrl($this->url);
        $helper->setScheme($scheme);
        $helper->setHost($host);
        $helper->setPort($port);
        $helper->setFragment($fragment);
        $helper->setPass($password);
        $helper->setUser($user);
        $helper->setQuery($query);
        $helper->setPath($path);
        $this->assertSame($scheme, $helper->getScheme());
        $this->assertSame($host, $helper->getHost());
        $this->assertSame($port, $helper->getPort());
        $this->assertSame($fragment, $helper->getFragment());
        $this->assertSame($password, $helper->getPass());
        $this->assertSame($user, $helper->getUser());
        $this->assertSame($query, $helper->getQuery());
        $this->assertSame($path, $helper->getPath());
    }
}
