<?php

declare(strict_types=1);

namespace Tests\Helpers;

use JTL\DB\NiceDB;
use JTL\Helpers\Seo;
use PHPUnit\Framework\TestCase;

class SeoTest extends TestCase
{
    public function testGetSeo(): void
    {
        $this->assertSame('', Seo::getSeo(123));
        $this->assertSame('', Seo::getSeo([1]));
        $this->assertSame('', Seo::getSeo(null));
        $this->assertSame('', Seo::getSeo(null, true));
        $this->assertSame('test', Seo::getSeo('test'));
        $this->assertSame('test', Seo::getSeo('test', true));
        $this->assertSame('test-foo', Seo::getSeo('test_foo'));
        $this->assertSame('test_foo', Seo::getSeo('test_foo', true));
        $this->assertSame('test$-.+(),foo', Seo::getSeo('test$–_.+!*‘(),foo'));
        $this->assertSame('test$_.+(),foo', Seo::getSeo('test$–_.+!*‘(),foo', true));
    }

    public function testCheckSeo(): void
    {
        $db = $this->createStub(NiceDB::class);
        $this->assertSame('', Seo::checkSeo(''));
        $this->assertSame('test', Seo::checkSeo('test', $db));
        $db->method('select')->willReturn((object)['cSeo' => 'test']);
        $db->method('getSingleObject')->willReturnOnConsecutiveCalls(
            (object)['newSeo' => 'testResult1'],
            null
        );
        $this->assertSame('testResult1', Seo::checkSeo('test', $db));
        $this->assertSame('test3', Seo::checkSeo('test3', $db));
    }

    public function testGetFlatSeoPath(): void
    {
        $this->assertSame('My-product-Name', Seo::getFlatSeoPath('My/product/Name'));
        $this->assertSame('MyProductName', Seo::getFlatSeoPath('MyProductName'));
    }
}
