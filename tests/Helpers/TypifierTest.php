<?php

declare(strict_types=1);

namespace Tests\Helpers;

use JTL\Helpers\Typifier;
use Tests\UnitTestCase;

class TypifierTest extends UnitTestCase
{
    public function testObjectify(): void
    {
        $resp = new \stdClass();
        $this->assertEquals($resp, Typifier::objectify(null, $resp));
        $this->assertEquals($resp, Typifier::objectify(null));
        $this->assertEquals($resp, Typifier::objectify([]));
        $this->assertEquals($resp, Typifier::objectify($resp));
        $this->assertEquals($resp, Typifier::objectify(22));
    }

    public function testIntifyOrNull(): void
    {
        $this->assertEquals(null, Typifier::intifyOrNull(null));
        $this->assertEquals(2, Typifier::intifyOrNull('2.123'));
    }

    public function testArrify(): void
    {
        $this->assertEquals([1], Typifier::arrify(1));
        $this->assertEquals([1], Typifier::arrify(null, [1]));
        $this->assertEquals([], Typifier::arrify(null));
    }

    public function testIntify(): void
    {
        $this->assertEquals(1, Typifier::intify(null, 1));
        $this->assertEquals(2, Typifier::intify('2.123', 3));
        $this->assertEquals(3, Typifier::intify(null, 3));
    }

    public function testTypeify(): void
    {
        $this->assertEquals(2, Typifier::typeify('2.34', 'integer'));
        $this->assertEquals(2.34, Typifier::typeify('2.34', 'float'));
        $this->assertEquals('2.34', Typifier::typeify('2.34', 'doesnotexist'));
    }

    public function testStringify(): void
    {
        $this->assertEquals('123', Typifier::stringify(123));
        $this->assertEquals('123', Typifier::stringify('123'));
        $this->assertEquals('', Typifier::stringify(null));
        $this->expectException(\InvalidArgumentException::class);
        Typifier::stringify([1], 'test');
    }

    public function testFloatify(): void
    {
        $this->assertEquals(0.00, Typifier::floatify(null));
        $this->assertEquals(1.00, Typifier::floatify(1));
        $this->assertEquals(3.145, Typifier::floatify('3.145'));
    }

    public function testBoolify(): void
    {
        $this->assertTrue(Typifier::boolify(1));
        $this->assertFalse(Typifier::boolify(0));
        $this->assertTrue(Typifier::boolify(true));
        $this->assertFalse(Typifier::boolify(false));
        $this->assertTrue(Typifier::boolify(1));
        $this->assertTrue(Typifier::boolify('Y'));
        $this->assertTrue(Typifier::boolify('yes'));
        $this->assertTrue(Typifier::boolify('ja'));
        $this->assertFalse(Typifier::boolify('n'));
        $this->assertFalse(Typifier::boolify('_blah'));
        $this->assertEquals(null, Typifier::boolify('_blah', null));
        $this->assertTrue(Typifier::boolify('_blah', true));
        $this->assertFalse(Typifier::boolify(null, null));
    }
}
