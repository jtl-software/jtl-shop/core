<?php

namespace Tests\TestHelper;

use JTL\Language\LanguageHelper;
use JTL\Language\LanguageModel;

class LanguageHelperExtended extends LanguageHelper
{
    public function __construct()
    {
    }

    /**
     * @return LanguageModel[]
     */
    public function getAvailable(): array
    {
        return $_SESSION['Sprachen'];
    }

    public function getLanguageID(): int
    {
        return 1;
    }

    public function get(string $cName, string $cSektion = 'global', mixed ...$arg1): string
    {
        return $cName . $cSektion;
    }
}
