<?php

declare(strict_types=1);

namespace Tests\TestHelper;

use ReflectionClass;
use ReflectionNamedType;
use ReflectionProperty;
use stdClass;

trait DataTransferObjectTrait
{
    public function generateDataForDTO(string $dtoClassName, array $customData = []): array
    {
        $reflect    = new ReflectionClass($dtoClassName);
        $properties = $reflect->getProperties(
            ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED
        );
        $data       = [];
        foreach ($properties as $property) {
            $propertyType = $property->getType();
            if ($propertyType instanceof ReflectionNamedType === false) {
                continue;
            }
            $data[$property->name] = match ($propertyType->getName()) {
                'int'                            => 1,
                'string'                         => '',
                'float'                          => 0.00,
                'bool'                           => true,
                'array'                          => [],
                'stdClass'                       => new stdClass(),
                default                          => null,
            };
        }
        foreach ($customData as $offset => $value) {
            $data[$offset] = $value;
        }

        return $data;
    }
}
